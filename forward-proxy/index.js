var express = require('express');
var bodyParser = require('body-parser');

var request = require('request');

var app = express();


app.use(bodyParser());

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.post('*', function(req, res){
	
	var body = req.body;
	body.appID = 'bocagentstaging';
	var konyparam = JSON.parse(body.konyreportingparams);

	konyparam.aid = 'bocagentstaging';
	body.konyreportingparams = konyparam;

	var url = req.url;
	url = url.replace('bocmyagentmobile', 'bocagentstaging');
	url = 'https://boclimited.konycloud.com'+url;
	
	console.log(body.serviceID);

	request.post({
		url: url,		
		form: body
	}).pipe(res);

});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});