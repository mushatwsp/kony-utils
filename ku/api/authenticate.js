var BaseAPI = require("./base_api");
var request = require("superagent");
var requestPromisePlugin = require("superagent-promise-plugin");

module.exports = class Authenticate extends BaseAPI{

  call(plant, username, password){
    var params = {
      plant_code: plant,
      username: username,
      password: password
    };
    console.log(this.base_url)
    return request.post(this.base_url+"/authenticate/?formate=json")
    .use(requestPromisePlugin)
    .set('Accept', 'application/json')
    .send(params);

  }

}
