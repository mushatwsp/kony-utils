
module.exports = class APIFactory{

  static createAPI(name){
    var APIClass = require("./"+name);
    return new APIClass("http://bocau.wspdigitalstaging.com/api/v1");
  }

}
