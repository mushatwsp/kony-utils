var APIFactory = require("../api/api_factory");

module.exports = function(authenticate_user, order_id, scan_package){
  return authenticate_user.then( (res) => {
    return APIFactory.createAPI("cc_put_scans").call(order_id, scan_package, res.body.token);
  });
}
