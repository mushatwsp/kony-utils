var APIFactory = require("../api/api_factory");

module.exports = function(plant, username, password){
  return APIFactory.createAPI("authenticate").call(plant, username, password);
};
