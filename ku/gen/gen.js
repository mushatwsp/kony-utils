#!/usr/bin/env node

var mustache = require('mustache');
var controller_view = require('./views/controller_view');
var fs = require('fs');
var program = require('commander');
var _ = require("underscore");
var path = require('path');

var generate = function(name, form, src){
  var data = controller_view(name, form, src);
  var mustache_file = fs.readFileSync("./templates/controller.mustache", 'utf8');
  return mustache.render(mustache_file, data);
}

program
  .version('0.0.1')
  .command('generate [controller_name] [form_name] [source_form_path]')
  .action(function (cname, form_name, src_form_path) {
    console.log(generate(cname, form_name, src_form_path));
  });

program
  .command("gen-all [source_dir] [out_dir]")
  .option("-p, --prefix <prefix>")
  .action(function(src_dir, out_dir, options){
    if(fs.existsSync(out_dir)){
      fs.rmdirSync(out_dir);
    }
    fs.mkdir(out_dir);
    var prefix = options.prefix || "frm";

    _
      .chain(fs.readdirSync(src_dir))
      .filter(function (p) {
        return p.startsWith(prefix);
      })
      .map(function(p){
        return path.join(src_dir, p);
      })
      .each(function(p){
        var file_name = path.basename(p);
        var form_name = file_name.replace(".js","");
        var con_name = file_name.replace(prefix, "").replace(".js","")+"Controller";

        var gen = generate(con_name, form_name, p);
        fs.writeFileSync(path.join(out_dir, con_name+".js"), gen, {encoding:"utf8"});
      })
  });

  program.parse(process.argv);
