var fs = require('fs');
var _ = require("underscore")

module.exports = function(controller_name, form, generated_form_name_path){
  //var btnShowPrices = new kony.ui.Button({
  var re = /var (.+) = new kony\.ui\.Button.*/g;
  var re1 = /var (.+) = new kony\.ui\.Button.*/;
  var content = fs.readFileSync(generated_form_name_path, 'utf8');
  var callbacks = _.map(content.match(re), function(m){
    return {
      name:m.match(re1)[1],
      event:"onClick"
    };
  });

  return {
    controller:controller_name,
    form:form,
    callbacks:callbacks
  };
};
