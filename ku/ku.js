#!/usr/bin/env node

var fs = require('fs');
var program = require('commander');
var _ = require("lodash");

program
  .version('0.0.1')
  .command('api [api_endpoint]')
  .option("-t, --token <token>", "token for the api")
  .action((endpoint, options) => {

    console.log(endpoint);
    console.log(options.token);

    APIFactory.createAPI("authenticate").call("A001", "mush","123").then((res) => {
      console.log(res.body.token);
    })
  });

program.parse(process.argv);
